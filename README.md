# AppGen UI 

User interface build with Node.js + Webkit 

https://github.com/rogerwang/node-webkit

Inspired from : http://codiqa.com/embed/editor

## Additionnal development tools

* Livereload : http://livereload.com/

* Sublime Text 2 with LiveReload plugin 

* Chrome with LiveReload plugin 

* Add NodeWebKit.sublime-build to sublime build system

		{
			"shell": true,
			"cmd": [ "taskkill", "/IM", "nw.exe", "&", "zip", "-r" , "../app.nw", "*", "&", "nw", "../app.nw"],
			"working_dir": "${project_path:${folder}}"
		}

Test with Ctrl + b.

